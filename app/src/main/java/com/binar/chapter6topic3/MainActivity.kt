package com.binar.chapter6topic3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var mDb: WeatherDataBase? = null
    private lateinit var mDbWorkerThread: DbWorkerThread

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mDbWorkerThread = DbWorkerThread("dbWorkerThread")
        mDbWorkerThread.start()

        mDb = WeatherDataBase.getInstance(this)

        btnSave.setOnClickListener{
            val task = Runnable {
                mDb?.weatherDataDao()
                        ?.insert(
                                WeatherData(null,etName.text.toString(),etRegion.text.toString(),etWeather.text.toString())
                        )
            }
            mDbWorkerThread.postTask(task)
            Toast.makeText(this,"Data Berhasil Disimpan",Toast.LENGTH_LONG).show()
        }

        btnChange.setOnClickListener {
            val task = Runnable {
                mDb?.weatherDataDao()
                    ?.update(WeatherData(etEditId.text.toString().toLong(),
                        etEditName.text.toString(),
                        etEditRegion.text.toString(),
                        etEditWeather.text.toString()
                    ))
            }
            mDbWorkerThread.postTask(task)
            Toast.makeText(this,"Data Berhasil Diubah",Toast.LENGTH_LONG).show()
        }

        btnShow.setOnClickListener {
            val task = Runnable {
                val listData = mDb?.weatherDataDao()?.getAll()
                if (listData != null && listData.isNotEmpty()) {
                    for(data in listData){
                        Log.d("BINAR","ID : ${data.id} Nama Cuaca : ${data.name} Daerah : ${data.region} Cuaca : ${data.weather}")
                    }
                    Toast.makeText(this,"Data Berhasil Didapatkan. LIHAT LOG DEBUG",Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(this,"Data KOSONG",Toast.LENGTH_LONG).show()
                }
            }
            mDbWorkerThread.postTask(task)
        }

        btnRemoveAll.setOnClickListener {
            val task = Runnable {
                mDb?.weatherDataDao()?.deleteAll()
            }
            mDbWorkerThread.postTask(task)
            Toast.makeText(this,"Semua data DIHAPUS",Toast.LENGTH_LONG).show()
        }
    }

    override fun onDestroy() {
        WeatherDataBase.destroyInstance()
        mDbWorkerThread.quit()
        super.onDestroy()
    }
}
