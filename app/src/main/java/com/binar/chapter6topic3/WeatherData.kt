package com.binar.chapter6topic3

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "weatherData")
data class WeatherData(@PrimaryKey(autoGenerate = true) var id: Long?,
                       @ColumnInfo(name = "name") var name: String,
                       @ColumnInfo(name = "region") var region: String,
                       @ColumnInfo(name = "weather") var weather: String)
{
    constructor():this(null,"","","")
}